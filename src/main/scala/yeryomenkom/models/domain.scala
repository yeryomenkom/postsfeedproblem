package yeryomenkom.models

import java.time.LocalDateTime
import java.util.UUID

object domain {

  type UserId = Int
  type GroupId = Int

  case class User(id: UserId, name: String)

  case class Group(id: GroupId, title: String)

  case class Post(id: UUID, groupId: Int, authorId: Int, content: String, created: LocalDateTime)

  object Post {
    implicit val LocalDateTimeOrdering: Ordering[LocalDateTime] = new Ordering[LocalDateTime] {
      override def compare(x: LocalDateTime, y: LocalDateTime): Int = x.compareTo(y)
    }

    val OrderingCreatedDESC: Ordering[Post] = Ordering.by[Post, LocalDateTime](_.created).reverse
  }

}

package yeryomenkom.models

import java.time.LocalDateTime
import java.util.UUID

object api {

  case class PostForGroup(id: UUID,
                          authorId: Int,
                          authorName: String,
                          created: LocalDateTime,
                          content: String)

  case class PostForUser(id: UUID,
                         groupId: Int,
                         authorId: Int,
                         authorName: String,
                         created: LocalDateTime,
                         content: String)

  case class RawPost(authorId: Int, content: String)

}

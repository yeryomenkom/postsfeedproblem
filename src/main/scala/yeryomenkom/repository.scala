package yeryomenkom

import java.util.concurrent.ConcurrentHashMap
import java.util.{UUID, concurrent}

import yeryomenkom.Contracts.WithId
import yeryomenkom.models.domain._

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}

object repository {

  /**
    * For the test task I've decided to use in memory storage.
    */
  class InMemoryRepositoryImpl[T, Id](implicit withId: WithId[T, Id], ec: ExecutionContext) {

    private val data: ConcurrentHashMap[Id, T] = new concurrent.ConcurrentHashMap()

    def loadAll: Future[Seq[T]] =
      Future { data.values().asScala.toSeq }

    def loadAll(ids: Set[Id]): Future[Seq[T]] =
      Future { ids.toSeq.flatMap(id => Option(data.get(id))) }

    def saveAll(values: Seq[T]): Future[Unit] =
      Future { values.foreach(value => data.put(withId.getId(value), value)) }

    def deleteAll(): Future[Unit] =
      Future { data.clear() }

    def query(predicate: T => Boolean): Future[Seq[T]] =
      loadAll.map(_.filter(predicate))

    def query(predicate: T => Boolean, sortBy: Ordering[T]): Future[Seq[T]] =
      query(predicate).map(_.sorted(sortBy))

  }

  class PostFeedRepository(implicit ec: ExecutionContext)
    extends InMemoryRepositoryImpl[Post, UUID]()(WithId.create(_.id), ec)

  class PostRepository(implicit ec: ExecutionContext)
    extends InMemoryRepositoryImpl[Post, UUID]()(WithId.create(_.id), ec)

  class UserRepository(implicit ec: ExecutionContext)
    extends InMemoryRepositoryImpl[User, UserId]()(WithId.create(_.id), ec)

  class GroupRepository(implicit ec: ExecutionContext)
    extends InMemoryRepositoryImpl[Group, GroupId]()(WithId.create(_.id), ec)

}
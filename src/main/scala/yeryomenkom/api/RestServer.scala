package yeryomenkom.api

import java.time.LocalDateTime

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import yeryomenkom.actors.groups.protocol.ErrorReason._
import yeryomenkom.actors.users.protocol.ErrorReason._
import yeryomenkom.models.api.RawPost

class RestServer(api: ApiService)(implicit system: ActorSystem, am: ActorMaterializer, t: Timeout) extends CirceSupport {
  import system.dispatcher

  def start(): Unit = {
    Http().bindAndHandle(routes, "localhost", 8080)
  }

  private implicit val myExceptionHandler: ExceptionHandler = ExceptionHandler {
    case UserNotFound | GroupNotFound => complete(StatusCodes.NotFound)
    case UserNotMember => complete(StatusCodes.Forbidden)
    case _ => complete(StatusCodes.InternalServerError)
  }

  private val routes: Route =
    path("user" / IntNumber / "groups") { userId =>
      get {
        complete(api.getUserGroups(userId))
      } ~
        put {
          parameters('group_id.as[Int]) { groupId =>
            complete(api.addMemberToGroup(groupId, userId))
          }
        }
    } ~
      path("user" / IntNumber / "postsfeed") { userId =>
        parameters('from.as[LocalDateTime], 'to.as[LocalDateTime]) { (from, to) =>
          complete(api.getUserPostsFeed(userId, from, to))
        }
      } ~
      path("group" / IntNumber / "postsfeed") { groupId =>
        get {
          parameters('from.as[LocalDateTime], 'to.as[LocalDateTime]) { (from, to) =>
            complete(api.getGroupPostsFeed(groupId, from, to))
          }
        } ~
          post {
            entity(as[RawPost]) { raw =>
              complete(api.addPost(groupId, raw))
            }
          }
      }
}
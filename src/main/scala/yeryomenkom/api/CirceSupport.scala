package yeryomenkom.api

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.UUID

import akka.http.scaladsl.unmarshalling.Unmarshaller
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.{Decoder, Encoder}
import io.circe.generic.AutoDerivation
import io.circe.parser._

import scala.concurrent.Future
import scala.util.Try

trait CirceSupport extends FailFastCirceSupport with AutoDerivation {

  implicit def unmarshallerFromString[T: Decoder]: Unmarshaller[String, T] =
    Unmarshaller { implicit ec => str =>
      Future.fromTry(decode[T](str).toTry)
    }

  private val Formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm")
  implicit val LocalDateTimeDecoder: Decoder[LocalDateTime] =
    Decoder[String].emapTry(str => Try { LocalDateTime.parse(str, Formatter) })
  implicit val LocalDateTimeEncoder: Encoder[LocalDateTime] =
    Encoder[String].contramap(Formatter.format)

  implicit val UUIDDecoder: Decoder[UUID] =
    Decoder[String].emapTry(str => Try { UUID.fromString(str) })
  implicit val UUIDEncoder: Encoder[UUID] =
    Encoder[String].contramap(_.toString)

}

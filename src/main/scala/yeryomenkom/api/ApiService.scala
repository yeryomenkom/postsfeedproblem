package yeryomenkom.api

import java.time.LocalDateTime
import java.util.UUID

import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import yeryomenkom.actors.groups.protocol._
import yeryomenkom.actors.users.protocol._
import yeryomenkom.models.api.{PostForGroup, PostForUser, RawPost}
import yeryomenkom.models.domain.{GroupId, Post, User, UserId}
import yeryomenkom.repository.UserRepository

import scala.concurrent.Future

class ApiService(groupManager: ActorRef, userManager: ActorRef, userRepository: UserRepository)
                (implicit as: ActorSystem, am: ActorMaterializer, t: Timeout) {

  import as.dispatcher

  private def failedIfException[A](future: Future[A]): Future[A] =
    future.flatMap {
      case err: Exception => Future.failed(err)
      case value => Future.successful(value)
    }

  private def createPost(raw: RawPost, groupId: GroupId): Post =
    Post(
      id = UUID.randomUUID(),
      groupId = groupId,
      authorId = raw.authorId,
      content = raw.content,
      created = LocalDateTime.now()
    )

  private def createPostForGroup(post: Post, user: User): PostForGroup =
    PostForGroup(
      id = post.id,
      authorId = user.id,
      authorName = user.name,
      post.created,
      content = post.content
    )

  private def createPostForUser(post: Post, user: User): PostForUser =
    PostForUser(
      id = post.id,
      groupId = post.groupId,
      authorId = user.id,
      authorName = user.name,
      post.created,
      content = post.content
    )

  def getUserGroups(userId: UserId): Future[Set[GroupId]] =
    failedIfException(userManager ? GetUserGroups(userId)) collect {
      case UserGroups(groupIds) => groupIds
    }

  def addMemberToGroup(groupId: GroupId, userId: UserId): Future[Unit] =
    failedIfException(userManager ? BecomeGroupMember(groupId, userId)) collect {
      case GroupMemberAdded(_, _) => ()
    }

  def addPost(groupId: GroupId, raw: RawPost): Future[UUID] =
    failedIfException(groupManager ? AddPost(createPost(raw, groupId))) collect {
      case PostAdded(post, _) => post.id
    }

  def getGroupPostsFeed(groupId: GroupId, from: LocalDateTime, to: LocalDateTime): Future[Seq[PostForGroup]] =
    for {
      posts <- failedIfException(groupManager ? GetGroupFeedPosts(groupId, from, to)) collect {
        case GroupFeedPosts(posts) => posts
      }
      users <- userRepository.loadAll(posts.map(_.authorId).toSet)
      userById = users.map(u => u.id -> u).toMap
    } yield posts.map(post => createPostForGroup(post, userById(post.authorId)))

  def getUserPostsFeed(userId: UserId, from: LocalDateTime, to: LocalDateTime): Future[Seq[PostForUser]] =
    for {
      posts <- failedIfException(userManager ? GetUserFeedPosts(userId, from, to)) collect {
        case UserFeedPosts(posts) => posts
      }
      users <- userRepository.loadAll(posts.map(_.authorId).toSet)
      userById = users.map(u => u.id -> u).toMap
    } yield posts.map(post => createPostForUser(post, userById(post.authorId)))

}
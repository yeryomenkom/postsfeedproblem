package yeryomenkom

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.util.Timeout
import yeryomenkom.actors.groups.GroupManagerActor
import yeryomenkom.actors.users.UserManagerActor
import yeryomenkom.api.{ApiService, RestServer}
import yeryomenkom.models.domain.{Group, User}
import yeryomenkom.repository.{GroupRepository, PostFeedRepository, PostRepository, UserRepository}

import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor}
import scala.concurrent.duration._


object Main extends App {

  object Executors {
    val blockingIO: ExecutionContextExecutor = ExecutionContext.global
  }

  /**
    * We use some NoSql solution (e.g. Cassandra) for storing all posts permanently.
    */
  val postRepository = new PostRepository()(Executors.blockingIO)

  /**
    * We can try to use some Sql solution (e.g. MySQL) for storing basic info about system agents.
    */
  val userRepository = new UserRepository()(Executors.blockingIO)
  val groupRepository = new GroupRepository()(Executors.blockingIO)

  val users = (1 to 10).map(i => User(i, s"User $i"))
  val groups = (1 to 10).map(i => Group(i, s"Group $i"))

  val storeDefaultData = userRepository.saveAll(users) zip groupRepository.saveAll(groups)
  Await.ready(storeDefaultData, 5.seconds)

  /**
    * We use some cache (e.g. Redis) for storing PostsFeed per each agent in the system.
    * Whenever we observe an event which can influence PostsFeed of any agent we should rebuild or update
    * corresponding cache entry.
    * It will help us to load PostsFeed for all agents in reasonable time.
    */
  val userFeedRepositories = groups.map(_.id -> new PostFeedRepository()(Executors.blockingIO)).toMap
  val groupFeedRepositories = groups.map(_.id -> new PostFeedRepository()(Executors.blockingIO)).toMap


  implicit val system: ActorSystem = ActorSystem("users_and_groups")

  val groupManager = system.actorOf(
    GroupManagerActor.props(groupRepository, postRepository, userFeedRepositories),
    "group_manager"
  )
  val userManager = system.actorOf(
    UserManagerActor.props(groupManager, userRepository, postRepository, userFeedRepositories),
    "user_manager"
  )


  val timeout = Timeout(5.seconds)
  val apiService = new ApiService(groupManager, userManager, userRepository)(system, ActorMaterializer(), timeout)
  val restServer = new RestServer(apiService)(system, ActorMaterializer(), timeout)
  restServer.start()

}

package yeryomenkom

object Contracts {

  trait WithId[T, Id] {
    def getId(value: T): Id
  }

  object WithId {
    def create[T, Id](getter: T => Id): WithId[T, Id] = new WithId[T, Id] {
      override def getId(value: T): Id = getter(value)
    }
  }

}

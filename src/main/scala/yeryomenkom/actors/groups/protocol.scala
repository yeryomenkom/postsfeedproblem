package yeryomenkom.actors.groups

import java.time.LocalDateTime

import akka.actor.ActorRef
import yeryomenkom.models.domain._

object protocol {

  sealed trait Event

  sealed trait Request
  case class StartGroupManager(groups: Seq[Group]) extends Request
  case class StartGroup(group: Group) extends Request
  case class AddGroupMember(groupId: GroupId, userId: UserId, user: ActorRef) extends Request
  case class AddPost(post: Post) extends Request
  case class GetGroupFeedPosts(groupId: GroupId, from: LocalDateTime, to: LocalDateTime) extends Request

  sealed trait Response
  case class GroupMemberAdded(groupId: GroupId, group: ActorRef) extends Response with Event
  case class PostAdded(post: Post, requester: ActorRef) extends Response with Event
  case class GroupFeedPosts(posts: Seq[Post]) extends Response

  sealed trait ErrorReason extends Exception
  object ErrorReason {
    case object GroupNotFound extends ErrorReason
    case object UserNotMember extends ErrorReason
  }

}

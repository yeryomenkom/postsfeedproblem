package yeryomenkom.actors.groups

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.pipe
import yeryomenkom.actors.groups.GroupManagerActor.{GroupActorCreator, State}
import yeryomenkom.actors.groups.protocol._
import yeryomenkom.models.domain.GroupId
import yeryomenkom.repository.{GroupRepository, PostFeedRepository, PostRepository}

class GroupManagerActor(groupRepository: GroupRepository, groupActorCreator: GroupActorCreator) extends Actor {
  import context._

  groupRepository.loadAll.map(StartGroupManager) pipeTo self

  override def receive: Receive = {
    case StartGroupManager(groups) =>
      val groupActors = groups
        .map { group =>
          val groupActor = system.actorOf(groupActorCreator(group.id), s"group_${group.id}")
          groupActor ! StartGroup(group)
          group.id -> groupActor
        }
        .toMap
      context.become(working(State(groupActors)))
  }

  private def working(state: State): Receive = {
      case msg: AddGroupMember =>
        forwardIfGroupExist(state, msg.groupId, msg)

      case msg: AddPost =>
        forwardIfGroupExist(state, msg.post.groupId, msg)

      case msg: GetGroupFeedPosts =>
        forwardIfGroupExist(state, msg.groupId, msg)
  }

  def forwardIfGroupExist(state: State, groupId: GroupId, msg: Any): Unit =
    state.groups.get(groupId).fold(sender ! ErrorReason.GroupNotFound)(_ forward msg)

}

object GroupManagerActor {

  type GroupActorCreator = GroupId => Props

  case class State(groups: Map[GroupId, ActorRef])

  def props(groupRepository: GroupRepository,
            postRepository: PostRepository,
            feedRepositories: GroupId => PostFeedRepository): Props = {
    val groupActorCreator = (id: GroupId) => {
      GroupActor.props(postRepository, feedRepositories(id))
    }
    Props(new GroupManagerActor(groupRepository, groupActorCreator))
  }

}
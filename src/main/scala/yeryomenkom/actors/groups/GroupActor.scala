package yeryomenkom.actors.groups

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.pipe
import yeryomenkom.actors.groups.GroupActor.State
import yeryomenkom.actors.groups.protocol._
import yeryomenkom.models.domain.{Group, Post, UserId}
import yeryomenkom.repository.{PostFeedRepository, PostRepository}

class GroupActor(postRepository: PostRepository, feedRepository: PostFeedRepository) extends Actor {
  import context._

  override def receive: Receive = {
    case StartGroup(group) =>
      context.become(working(State(group, Map.empty)))
  }

  private def working(state: State): Receive = {

    case AddGroupMember(_, userId, user) =>
      val members = state.members + (userId -> user)
      context.become(working(state.copy(members = members)))
      val msg = GroupMemberAdded(state.info.id, self)
      sender ! msg
      user ! msg

    case AddPost(post) =>
      if (!state.members.contains(post.authorId)) sender ! ErrorReason.UserNotMember
      else {
        val requester = sender()
        postRepository.saveAll(Seq(post)).map(_ => PostAdded(post, requester)) pipeTo self
      }

    case msg: PostAdded =>
      msg.requester ! msg
      state.members.valuesIterator.foreach(_ ! msg)
      feedRepository.saveAll(Seq(msg.post))

    case GetGroupFeedPosts(_, from, to) =>
      feedRepository.query(post => from.isBefore(post.created) && to.isAfter(post.created), Post.OrderingCreatedDESC)
        .map(GroupFeedPosts) pipeTo sender()

  }

}

object GroupActor {

  case class State(info: Group, members: Map[UserId, ActorRef])

  def props(postRepository: PostRepository, feedRepository: PostFeedRepository): Props =
    Props(new GroupActor(postRepository, feedRepository))

}
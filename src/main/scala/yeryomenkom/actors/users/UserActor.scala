package yeryomenkom.actors.users

import akka.actor.{Actor, Props}
import akka.pattern.pipe
import yeryomenkom.actors.groups.protocol.{GroupMemberAdded, PostAdded}
import yeryomenkom.actors.users.UserActor.State
import yeryomenkom.actors.users.protocol._
import yeryomenkom.models.domain.{GroupId, Post, User}
import yeryomenkom.repository.{PostFeedRepository, PostRepository}

import scala.concurrent.Future

class UserActor(postRepository: PostRepository, feedRepository: PostFeedRepository) extends Actor {
  import context._

  override def receive: Receive = {
    case StartUser(user) =>
      context.become(working(State(user, Set.empty)))
  }

  private def working(state: State): Receive = {
    case PostAdded(post, _) =>
      feedRepository.saveAll(Seq(post))

    case GroupMemberAdded(groupId, _) =>
      val groupIds = state.groupIds + groupId
      recalculateFeed(groupIds)
      context.become(working(state.copy(groupIds = groupIds)))

    case GetUserFeedPosts(_, from, to) =>
      feedRepository.query(post => from.isBefore(post.created) && to.isAfter(post.created), Post.OrderingCreatedDESC)
        .map(UserFeedPosts) pipeTo sender()

    case GetUserGroups(_) =>
      sender() ! UserGroups(state.groupIds)
  }

  /**
    * It is very naive approach. In real case scenario we should have more optimized logic and that logic
    * should live in another actor.
    */
  private def recalculateFeed(groupIds: Set[GroupId]): Future[Unit] =
    for {
      _ <- feedRepository.deleteAll()
      postsToSave <- postRepository.query(p => groupIds.contains(p.groupId))
      _ <- feedRepository.saveAll(postsToSave)
    } yield ()

}

object UserActor {

  case class State(info: User, groupIds: Set[GroupId])

  def props(postRepository: PostRepository, feedRepository: PostFeedRepository): Props =
    Props(new UserActor(postRepository, feedRepository))

}
package yeryomenkom.actors.users

import java.time.LocalDateTime

import yeryomenkom.models.domain._

object protocol {

  sealed trait Request
  case class StartUserManager(users: Seq[User]) extends Request
  case class StartUser(user: User) extends Request
  case class BecomeGroupMember(groupId: GroupId, userId: UserId) extends Request
  case class GetUserFeedPosts(userId: UserId, from: LocalDateTime, to: LocalDateTime) extends Request
  case class GetUserGroups(userId: UserId) extends Request

  sealed trait Response
  case class UserFeedPosts(posts: Seq[Post]) extends Response
  case class UserGroups(groupIds: Set[GroupId]) extends Response


  sealed trait ErrorReason extends Exception
  object ErrorReason {
    case object UserNotFound extends ErrorReason
  }
}

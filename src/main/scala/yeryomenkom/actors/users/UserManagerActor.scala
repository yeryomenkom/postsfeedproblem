package yeryomenkom.actors.users

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.pipe
import yeryomenkom.actors.groups.protocol.AddGroupMember
import yeryomenkom.actors.users.UserManagerActor.{State, UserActorCreator}
import yeryomenkom.actors.users.protocol._
import yeryomenkom.models.domain.UserId
import yeryomenkom.repository.{PostFeedRepository, PostRepository, UserRepository}

class UserManagerActor(groupManager: ActorRef,
                       userRepository: UserRepository,
                       userActorCreator: UserActorCreator) extends Actor {
  import context._

  userRepository.loadAll.map(StartUserManager) pipeTo self

  override def receive: Receive = {
    case StartUserManager(users) =>
      val userActors = users
        .map { user =>
          val userActor = system.actorOf(userActorCreator(user.id), s"user_${user.id}")
          userActor ! StartUser(user)
          user.id -> userActor
        }
        .toMap
      context.become(working(State(userActors)))
  }

  private def working(state: State): Receive = {
    case BecomeGroupMember(groupId, userId) =>
      doIfUserExist(state, userId) { user => groupManager forward AddGroupMember(groupId, userId, user) }

    case msg: GetUserFeedPosts =>
      forwardIfUserExist(state, msg.userId, msg)

    case msg: GetUserGroups =>
      forwardIfUserExist(state, msg.userId, msg)
  }

  def doIfUserExist(state: State, userId: UserId)(actions: ActorRef => Unit): Unit =
    state.users.get(userId).fold(sender ! ErrorReason.UserNotFound)(actions)

  def forwardIfUserExist(state: State, userId: UserId, msg: Any): Unit =
    doIfUserExist(state, userId) { _ forward msg }

}

object UserManagerActor {

  type UserActorCreator = UserId => Props

  case class State(users: Map[UserId, ActorRef])

  def props(groupManager: ActorRef,
            userRepository: UserRepository,
            postRepository: PostRepository,
            feedRepositories: UserId => PostFeedRepository): Props = {
    val userActorCreator = (id: UserId) => {
      UserActor.props(postRepository, feedRepositories(id))
    }
    Props(new UserManagerActor(groupManager, userRepository, userActorCreator))
  }
}
